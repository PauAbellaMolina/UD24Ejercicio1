DROP table IF EXISTS empleados;

CREATE TABLE `empleados` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  `apellido` VARCHAR(100) NULL,
  `trabajo` ENUM('administrativo', 'publicista', 'programador') NULL,
  `salario` INT NULL,
  PRIMARY KEY (`id`));

insert into empleados (nombre, apellido, trabajo, salario)values('Pau', 'Abella', 'programador', 3000);
insert into empleados (nombre, apellido, trabajo, salario)values('Marc', 'Molina', 'administrativo', 1000);