package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Empleado;
import es.http.service.service.EmpleadoServiceImpl;

@RestController
@RequestMapping("/api")
public class EmpleadoController {
	
	@Autowired
	EmpleadoServiceImpl empleadoServiceImpl;
	
	@GetMapping("/empleados")
	public List<Empleado> listarEmpleadosController(){
		return empleadoServiceImpl.listarEmpleados();
	}
	
	@PostMapping("/empleados")
	public Empleado guardarEmpleadoController(@RequestBody Empleado empleado){
		return empleadoServiceImpl.guardarEmpleado(empleado);
	}
	
	@GetMapping("/empleados/{id}")
	public Empleado encontrarEmpleadoIdController(@PathVariable(name="id") Long id){
		Empleado empleadoEncontrado = new Empleado();
		empleadoEncontrado = empleadoServiceImpl.encontrarEmpleadoId(id);
		return empleadoEncontrado;
	}
	
	@GetMapping("/empleados/trabajos/{trabajo}")
	public List<Empleado> encontrarEmpleadoTrabajoController(@PathVariable(name="trabajo") String trabajo){
		return empleadoServiceImpl.encontrarEmpleadoTrabajo(trabajo);
	}
	
	@PutMapping("/empleados/{id}")
	public Empleado actualizarEmpleadoController(@PathVariable(name="id") Long id, @RequestBody Empleado empleado){
		Empleado empleadoSeleccionado = new Empleado();
		Empleado empleadoActualizado = new Empleado();
		
		empleadoSeleccionado = empleadoServiceImpl.encontrarEmpleadoId(id);
		
		empleadoSeleccionado.setNombre(empleado.getNombre());
		empleadoSeleccionado.setApellido(empleado.getApellido());
		empleadoSeleccionado.setTrabajo(empleado.getTrabajo());
		empleadoSeleccionado.setSalario(Empleado.calculadorSalario(empleado.getTrabajo()));
		
		empleadoActualizado = empleadoServiceImpl.actualizarEmpleado(empleadoSeleccionado);
		
		return empleadoActualizado;
	}
	
	@DeleteMapping("/empleados/{id}")
	public void eliminarEmpleadoController(@PathVariable(name="id") Long id){
		empleadoServiceImpl.eliminarEmpleado(id);
	}
}
