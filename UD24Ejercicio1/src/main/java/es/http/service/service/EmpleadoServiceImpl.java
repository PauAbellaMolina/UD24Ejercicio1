package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IEmpleadoDao;
import es.http.service.dto.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{

	@Autowired
	IEmpleadoDao iEmpleadoDao;
	
	@Override
	public List<Empleado> listarEmpleados() {
		return iEmpleadoDao.findAll();
	}
	
	@Override
	public Empleado guardarEmpleado(Empleado empleado) {
		empleado.setSalario(Empleado.calculadorSalario(empleado.getTrabajo()));
		return iEmpleadoDao.save(empleado);
	}
	
	@Override
	public Empleado encontrarEmpleadoId(Long id) {
		return iEmpleadoDao.findById(id).get();
	}
	
	@Override
	public List<Empleado> encontrarEmpleadoTrabajo(String trabajo) {
		return iEmpleadoDao.findByTrabajo(trabajo);
	}
	
	@Override
	public Empleado actualizarEmpleado(Empleado empleado) {
		return iEmpleadoDao.save(empleado);
	}
	
	@Override
	public void eliminarEmpleado(Long id) {
		iEmpleadoDao.deleteById(id);
	}
}
