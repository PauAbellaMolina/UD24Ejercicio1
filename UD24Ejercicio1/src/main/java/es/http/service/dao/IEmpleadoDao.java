package es.http.service.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import es.http.service.dto.Empleado;

public interface IEmpleadoDao extends JpaRepository<Empleado, Long>{
	public List<Empleado> findByTrabajo(String trabajo);
}
