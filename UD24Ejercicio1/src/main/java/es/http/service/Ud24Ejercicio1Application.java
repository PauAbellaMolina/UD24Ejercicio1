package es.http.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud24Ejercicio1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud24Ejercicio1Application.class, args);
	}

}
